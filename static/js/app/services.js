brandsServices.factory('messenger', function() {
    return {
        makeErrorMessage: function(msg) {
            return '<span class="validation-pimp"></span>' +
                '<div class="validation-message tooltip fade bottom in">' +
                '<div class="tooltip-arrow"></div>' + '<div class="tooltip-inner">' +
                msg + '</div>' + '</div>';
        }
    }
});

brandsServices.value('notifier', toastr);