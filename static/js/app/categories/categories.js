/**
 * Created by kozhuhds on 8/22/14.
 */

angular.module('brands.categories', [])
    .config(function ($routeProvider) {
        $routeProvider.
            when('/categories', {
                templateUrl: 'static/js/app/views/categories.html',
                controller: 'CategoryCtrl'
            }).
            when('/categories/:sex/:path', {
                templateUrl: 'static/js/app/views/categories.html',
                controller: 'ActiveCategoryCtrl'
            });
    });

angular.module('brands.categories')
    .factory('categories', function ($resource, $routeParams, apiUrl, $cookies, $location) {
        return {
            root: {},
            dir: null,
            dirName:'Категории',
            path: function (item) {
                var path = $routeParams.path ? $routeParams.path +'-'+  item.id : item.id;
                if( !this.getCurrSubcategoryById(item.id).length ) {
                    path = $routeParams.path +'-'+  item.id + '/products';
                }
                return path;
            },

            getGoodsCount: function (category) {
                /*var dir = $.extend({}, category.childNodes),
                    sum = 0,
                    self = this;
                if (!Object.keys(dir).length){
                    return category.countGoods;
                }
                angular.forEach(dir, function (item, key) {
                    sum += self.getGoodsCount(item);
                });*/

                var sum = category.categoryToUser[0] ? category.categoryToUser[0].count : 0;
                return sum;
            },
            fetch: function (cb, action) {
                var self = this;
                if ( self.root.length && action != 'refresh' ) {
                    cb && cb();
                    return ;
                }
                return $resource(apiUrl + 'categories/').get().$promise.then(function (categoryCollection) {
                    self.root = categoryCollection.categories;
                    !$routeParams.path && (self.dir = categoryCollection.categories);
                    cb && cb();
                });
            },
            goBack: function () {
                $location.url('/categories/' + $routeParams.path.split('-').slice(0,-1));
            },
            changeDir: function () {
                this.dir = this.root;
                if (!$routeParams.path) {
                    return
                }
                var self = this,
                    pages = $routeParams.path.split('-');
                angular.forEach(pages, function (id, key){
                    self.dir = self.getCurrSubcategoryById(id, true);
                });
            },
            getCurrSubcategoryById: function (id, changeName) {
                var result,
                    self = this;
                !this.dir && (this.dir = this.root);
                angular.forEach(this.dir, function (category, key){
                    if (category.id == id) {
                        if(changeName) {
                            self.dirName = category.name;
                        }
                        result = category.childNodes;
                        return false;
                    }
                });
                return result;
            }
        }
    });
