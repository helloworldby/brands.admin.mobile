/**
 * Created by kozhuhds on 8/22/14.
 */

angular.module('brands.categories')
    .controller('CategoryCtrl', function ($scope, categories, $rootScope, $routeParams) {
        categories.changeDir();
        categories.fetch(function (){
            $rootScope.loading = false;

            var categoriesModify = [];
            $.each(categories.root, function(k, val){
                if(val.internalId != 'aksessuary') {
                    var catModify = angular.copy(val);
                    catModify.name = catModify.name + ' мужская';
                    catModify.isForMale = 1;
                    categoriesModify.push(catModify);

                    var catModify = angular.copy(val);
                    catModify.name = catModify.name + ' женская';
                    catModify.isForFemale = 1;
                    categoriesModify.push(catModify);
                } else {
                    var catModify = angular.copy(val);
                    catModify.isForFemale = 1;
                    catModify.isForMale = 1;
                    categoriesModify.push(catModify);
                }
            })

            categories.dir = categoriesModify;
            console.log(categories.dir)
        });



        categories.dirName = 'Каталог';
        $scope.categories = categories;
        $scope.sexFilterValue = $routeParams.sex ? $routeParams.sex : null;



    })
    .controller('ActiveCategoryCtrl', function ($scope, categories, $rootScope, $routeParams) {
        categories.fetch(function () {
            categories.changeDir();
            $rootScope.loading = false;
        });
        $scope.categories = categories;
        $scope.sexFilterValue = $routeParams.sex ? $routeParams.sex : null;
    });