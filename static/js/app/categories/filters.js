angular.module('brands.categories')
    .filter('withProducts', function () {
        return function (collection, flag) {
            var out = [];

            angular.forEach(collection, function (item) {
                if(flag) {
                    item.categoryToUser && item.categoryToUser[0] && item.categoryToUser[0].count && out.push(item);
                } else {
                    (!item.categoryToUser || !item.categoryToUser[0] || !item.categoryToUser[0].count) && out.push(item);
                }
            });

            return out;
        };
    })
    .filter('sex', function () {
        return function (collection, sex) {
            var out = [];

            angular.forEach(collection, function (item) {
                if(sex == 'male') {
                    item.isForMale && out.push(item);
                } else if (sex == 'female') {
                    item.isForFemale && out.push(item);
                } else {
                    out.push(item);
                }
            });

            return out;
        };
    });