brands.controller('appCtrl', function ($scope, $rootScope, auth, settings, $cookieStore) {
    $scope.goBack = function () {
        window.history.back();
    };
    $scope.auth = auth;

    $rootScope.$on("$routeChangeStart", function(){
        $rootScope.loading = true;
    });

    // подтягиваем информацию о текущем пользователе и его магазине
    if($cookieStore.token) {
        settings.fetch(function(resp) {
            $rootScope.user = resp.user;
            $rootScope.shop = resp.user.shops[0];
        })
    }
});

brands.run(['$rootScope', function($rootScope) {
    $rootScope.loading = true;

}]);