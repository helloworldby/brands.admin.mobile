brandsDirectives
    .directive('formValidation', function ($timeout, messenger) {
    return {
        restrict: 'A',
        scope   : {
            formValidationClick: '&'
        },
        link: function postLink(scope, element, attrs) {

            jQuery.$firstError = null;

            function highlightHandler(element, errorClass, validClass) {
                var $element = $(element),
                    $container = $element.parents('.form-control-wrapper').first(),
                    timer = null,
                    delay = 3000;

                $container.removeClass(validClass + ' expired').addClass('error');
                $element.removeClass(validClass + ' expired').addClass('error');

                clearTimeout(timer);
                timer = null;

                if (jQuery.$firstError &&
                    (jQuery.$firstError.get(0) !== $element.get(0))) {
                    $container.addClass('expired');
                } else {
                    timer = setTimeout(function () {
                        $container.addClass('expired');
                    }, delay);
                }

            }

            function unhighlightHandler(element, errorClass, validClass) {
                var $element = $(element);
                $element.removeClass('error').addClass(validClass);
                $element.next().remove();
            }

            function successHandler(element) {
                function checkPosition(elem) {
                    var winWidth = $(window).width(),
                        $errorContainer = $(elem),
                        $pimp = $errorContainer.find('.validation-pimp'),
                        $errorTooltip = $errorContainer.find('.validation-message'),
                        errorWidth = $errorTooltip.outerWidth(true),
                        errorCutOff = (($pimp.offset().left +
                            errorWidth) > winWidth);

                    if (errorCutOff) {
                        $errorTooltip.removeClass('right').addClass('top-left top');
                    } else {
                        $errorTooltip.removeClass('top-left top').addClass('bottom');
                    }
                }

                // применяем позицию при ошибке
                if ($(element).siblings('.error-message').length) {
                    checkPosition($(element).siblings('.error-message'));
                }

                function resize() {
                    $('.control-group.error').each(function () {
                        checkPosition($(this).find('.error-message'));
                    });
                }

                // применяем позицию при ресайзе

                $(window).resize(resize);
            }

            function focusHandler(element) {
                jQuery.$firstError = null;
            }

            function invalidSubmitHandler(form, validator) {

                if (!validator.numberOfInvalids()) {
                    return;
                }

                $("html, body").bind("scroll mousedown DOMMouseScroll mousewheel keyup",
                    function () {
                        $('html, body').stop();
                    });


                $('html, body').animate({
                    scrollTop: $(validator.errorList[0].element).offset().top - 160
                }, 334, function () {
                    $("html, body").unbind("scroll mousedown DOMMouseScroll mousewheel keyup");
                });

                jQuery.$firstError = $(validator.errorList[0].element);

            }

            jQuery.validator.addMethod("fileRequired", function(value, element, params) {
                var model = scope.$parent.products.form['goods[images]'];
                if (model.constructor !== Array){
                    model.length = Object.keys(model).length;
                }
                return model.length ? true : false;
            });

            var rules = {
                    currencyField: {digits: true},
                    imageField: {fileRequired: true},
                    passSecond: {equalTo:"#inputPassFirst"}
                },
                form;
            $timeout(function () {
                /**
                 * Прицепить плагин валидации к форме
                 * @type {*|jQuery|HTMLElement}
                 */
                form = $(attrs.formValidationForm);
                form.validate({
                    onsubmit      : true,
                    errorClass    : 'error-message',
                    errorElement  : 'span',
                    onfocusout    : false,
                    onkeyup       : function(element) {
                        $(element).valid();
                    },
                    highlight     : highlightHandler,
                    onSuccess     : successHandler,
                    unhighlight   : unhighlightHandler,
                    invalidHandler: invalidSubmitHandler,
                    rules: rules,
                    ignore: '.hidden'
                });
            }, 0, false);

            jQuery.extend(jQuery.validator.messages, {
                digits: messenger.makeErrorMessage("Это поле должно содержать только цифры."),
                min: messenger.makeErrorMessage("Please enter a value greater than."),
                required: messenger.makeErrorMessage("Пожалуйста, заполните поле."),
                email: messenger.makeErrorMessage("Пожалуйста, введите правильный email."),
                fileRequired:  messenger.makeErrorMessage("Требуется хотя бы одно фото."),
                equalTo:  messenger.makeErrorMessage("Ошибка при повторе пароля.")
            });


            element.on('click', function (e) {
                if (form.valid()) {
                    scope.formValidationClick();
                }
            });
        }
    };
})
    .directive('brTimeRangeInput', function () {
        return {
            link: function (scope, elem, attrs) {
                $.mask.definitions['h'] = "[0-2]";
                $.mask.definitions['m'] = "[0-5]";
                elem.mask("h9:m9 - h9:m9");
            }
        }
    });
