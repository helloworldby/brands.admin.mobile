/**
 * Created by kozhuhds on 8/22/14.
 */


angular.module('brands.products', [])
    .config(function ($routeProvider) {
        $routeProvider.
            when('/categories/:sex/:path/products', {
                templateUrl: 'static/js/app/views/products-list.html',
                controller: 'ProductCtrl'
            }).
            when('/categories/:sex/:path/products/create', {
                templateUrl: 'static/js/app/views/product-form.html',
                controller: 'CreateProductCtrl'
            }).
            when('/categories/:sex/:path/products/:id/edit', {
                templateUrl: 'static/js/app/views/product-form.html',
                controller: 'EditProductCtrl'
            });
    });

angular.module('brands.products')
    .factory('products', function ($resource, $routeParams, apiUrl, $rootScope, categories) {
        return {
            form: {
                "goods[currency]": 22,
                'goods[images]': [],
                'goods[isForMale]': true
            },

            clearForm: function () {
                this.form = {
                    "goods[currency]": 22,
                    'goods[images]': []
                };
                this.sizes = {};
                $('.image-preview_item').remove();
            },

            fetch: function (cb) {
                var self = this,
                    categoryID = $routeParams.path.split('-').slice(-1).join();

                self.clearForm();
                return $resource(apiUrl + 'categories/' + categoryID + '/goods').get().$promise.then(function (productCollection) {
                    self.moderatedGoods = productCollection.moderatedGoods;
                    angular.forEach(self.moderatedGoods, function (item, key) {
                        item.arrangedSizes = {};
                        angular.forEach(item.sizes, function (size, key) {
                            item.arrangedSizes[size.id] = true;
                        });
                    });
                    self.notModeratedGoods = productCollection.notModeratedGoods;
                    self.rejectedGoods = productCollection.rejectedGoods;


                    cb && cb();
                    $rootScope.loading = false;

                });
            },

            edit: function (item) {
                var self = this;
                this.isEditing && clearTimeout(this.isEditing);
                this.isEditing = setTimeout(function () {
                    self.form = {
                        "goods[currency]": item.currency.id,
                        "goods[name]": item.name,
                        "goods[price]": item.price,
                        "goods[description]": item.description,
                        "goods[category]": item.category.id
                    };

                    if(item.inStock) {
                        self.form['goods[inStock]'] = 1;
                    } else {
                        delete self.form['goods[inStock]'];
                    }
                    if(item.isForMale) {
                        self.form['goods[isForMale]'] = 1;
                    } else {
                        delete self.form['goods[isForMale]'];
                    }
                    if(item.isForFemale) {
                        self.form['goods[isForFemale]'] = 1;
                    } else {
                        delete self.form['goods[isForFemale]'];
                    }

                    self.form["goods[sizes]"] = [];
                    angular.forEach(item.arrangedSizes, function (size, key) {
                        size && self.form["goods[sizes]"].push(key);
                    });

                    self.form["goods[images]"] = [];
                    angular.forEach(item.photos, function (photo, key) {
                        self.form["goods[images]"].push(photo.src);
                        if (photo.isMain == "1") {
                            self.form["goods[mainImage]"] = photo.src;
                        }
                    });

                    var Product = $resource(apiUrl + 'goods/' + item.id),
                        newProduct = new Product(self.form);
                    newProduct.$save(function () {
                        window.location.hash = window.location.hash.split('/').slice(0,5).join('/');
                        self.clearForm();
                    });
                }, 400);
            },

            fetchProduct: function (id, cb) {
                var self = this;
                return $resource(apiUrl + 'goods/'+ id).get().$promise.then(function (response) {
                    self.form = {
                        "goods[currency]": response.goods.currency.id,
                        "goods[name]": response.goods.name,
                        "goods[price]": response.goods.price,
                        "goods[description]": response.goods.description,
                        "goods[category]": response.goods.category.id,
                        "goods[inStock]": response.goods.inStock,
                        "goods[isForMale]": response.goods.isForMale,
                        "goods[isForFemale]": response.goods.isForFemale
                    };
                    self.sizes = [];
                    angular.forEach(response.goods.sizes, function (size, key) {
                        self.sizes[size.id] = true;
                    });
                    self.form['goods[images]'] = [];
                    angular.forEach(response.goods.photos, function (image, key) {
                        self.form['goods[images]'].push(image.src);
                    });
                    self.photos = response.goods.photos;
                    $rootScope.$broadcast('productload');

                    cb && cb();
                });
            },

            fetchFormInfo: function (cb) {
                var self = this,
                    categoryID = $routeParams.path.split('-').slice(-1);
                return $resource(apiUrl + 'categories/'+ categoryID +'/create-goods-info').get().$promise.then(function (response) {
                    self.formInfo = response;
                    cb && cb();
                });
            },

            save: function (addMore) {
                var self = this;
                self.form['goods[sizes]'] = [];
                self.form['goods[category]'] = $routeParams.path.split('-').slice(-1).join();
                if(self.form['goods[inStock]']) {
                    self.form['goods[inStock]'] = 1;
                } else {
                    delete self.form['goods[inStock]'];
                }

                if(self.form["goods[isForMale]"]) {
                    self.form["goods[isForMale]"] = 1;
                } else {
                    delete self.form["goods[isForMale]"];
                }
                if(self.form["goods[isForFemale]"]) {
                    self.form["goods[isForFemale]"] = 1;
                } else {
                    delete self.form["goods[isForFemale]"];
                }


                angular.forEach(this.sizes, function (val, key) {
                    val && self.form['goods[sizes]'].push(key);
                });
                var id = $routeParams.id || '',
                    Product = $resource(apiUrl + 'goods/' + id),
                    newProduct = new Product(this.form);
                newProduct.$save(function () {
                    if(!addMore) {
                        window.location.hash = window.location.hash.split('/').slice(0,5).join('/');
                        categories.fetch(null, 'refresh');
                    }
                    self.clearForm();

                });
            },
            delete: function () {
                var Product = $resource(apiUrl + 'goods/' + $routeParams.id),
                    currProduct = new Product();
                currProduct.$delete(function () {
                    window.location.hash = window.location.hash.split('/').slice(0,5).join('/');
                });
            }
        }
    });


