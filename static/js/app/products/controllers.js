/**
 * Created by kozhuhds on 8/22/14.
 */


angular.module('brands.products')
    .controller('ProductCtrl', function ($scope, products) {
        products.formInfo = {category: {name : ''}};
        $scope.currUrl = window.location.hash;
        products.fetch();
        products.fetchFormInfo();
        $scope.products = products;
        $scope.back = function () {
            var hashArr = window.location.hash.split('/');
            if(hashArr[hashArr.length-1] == 'products') {
                var categoriesList = hashArr[hashArr.length-2].split('-').slice(0,-1);
                if(categoriesList) {
                    window.location.hash = hashArr.slice(0, -2).join('/') + '/' + categoriesList.join('-');
                }
            } else {
                window.location.hash = window.location.hash.split('/').slice(0, -1).join('/');
            }
        };
    })
    .controller('CreateProductCtrl', function ($scope, products, $rootScope) {
        $scope.isCreate = true;
        $scope.addMore = false;
        products.fetchFormInfo(function () {
            $rootScope.loading = false;

            var category = products.formInfo.category;
            if(category.isForMale != category.isForFemale) {
                $scope.checkChoose('male', category.isForMale);
                $scope.checkChoose('female', !category.isForMale);
            } else {
                $scope.products.form['goods[isForMale]'] = true;
                $scope.products.form['goods[isForFemale]'] = true;
            }
        });
        $scope.title = "Новый товар";
        $scope.products = products;
        $scope.back = function () {
            window.location.hash = window.location.hash.split('/').slice(0, -1).join('/');
        };
        $scope.checkChoose = function (type) {
            if (!products.form['goods[isForFemale]'] && !products.form['goods[isForFemale]']) {
                if (type == 'male') {
                    products.form['goods[isForFemale]'] = true;
                } else {
                    products.form['goods[isForMale]'] = true;
                }
            }
        };
    })
    .controller('EditProductCtrl', function ($scope, products, $rootScope, $routeParams) {
        $scope.isEdit = true;
        $scope.title = "Редактирование товара";
        products.fetchProduct($routeParams.id, function () {
            $rootScope.loading = false;
        });
        products.fetchFormInfo();
        $scope.products = products;
        $scope.back = function () {
            window.location.hash = window.location.hash.split('/').slice(0, -2).join('/');
        };

    });
