/**
 * Created by kozhuhds on 9/11/14.
 */



angular.module('brands.settings', [])
    .config(function ($routeProvider) {
        $routeProvider.
            when('/settings', {
                templateUrl: 'static/js/app/views/settings-form.html',
                controller: 'SettingsCtrl'
            }).
            when('/settings/password', {
                templateUrl: 'static/js/app/views/settings-pass-form.html',
                controller: 'SettingsPassCtrl'
            });
    });


angular.module('brands.settings')
    .factory('settings', function ($resource, apiUrl, $rootScope, messenger, notifier, subscriptionTypeShop) {
        return {
            form: {},
            save: function () {
                var Settings = $resource(apiUrl + 'users/shop'),
                    newSettings = new Settings(this.form);
                newSettings.$save(function () {
                    notifier.success("Сохранено");
                });
            },
            savePassword: function () {
                var Password = $resource(apiUrl + 'users/change-password'),
                    newPassword = new Password(this.passform),
                    self = this;
                newPassword.$save(function () {
                    self.showPassForm = false;
                    self.passform = {};
                    notifier.success("Пароль изменен");
                }, function(resp) {
                    if (resp.data.errors.children.current_password.errors.length) {
                        $('#pass-form').data('validator').showErrors({
                            "currentPass": messenger.makeErrorMessage(resp.data.errors.children.current_password.errors[0])
                        });
                    };
                });
            },
            fetch: function (cb) {
                return $resource(apiUrl + 'users/shop').get().$promise.then(function (resp) {
                    $rootScope.$broadcast('shopload');
                    cb && cb(resp);
                });
            },
            initSettingsForm: function(cb) {
                var self = this;
                self.user = {};
                this.fetch(function(resp) {
                    self.days = {};
                    self.form = {
                        'user[email]': resp.user.email,
                        'user[shops][0][workingTime]': {}
                    };

                    if (resp.user.shops[0]) {
                        angular.extend(self.form, {
                            'user[shops][0][name]': resp.user.shops[0].name,
                            'user[shops][0][address]': resp.user.shops[0].address
                        });
                        if (resp.user.shops[0].logo) {
                            self.form['user[shops][0][logo]'] = resp.user.shops[0].logo.original_name;
                            self.logo = resp.user.shops[0].logo.path;
                        }
                        if(resp.user.shops[0].subscriptionType && resp.user.shops[0].subscriptionType.internalId == subscriptionTypeShop) {
                            self.form['user[shops][0][externalCatalogLink]'] = resp.user.shops[0].externalCatalogLink;
                        }

                        angular.forEach(resp.user.shops[0].workingTime, function (time, day) {
                            self.form['user[shops][0][workingTime]'][day] = time;
                            self.days[day] = true;
                        });
                    }

                    cb && cb(resp);
                })
            },
            changeDay: function (index, value, defaultTime) {
                this.form['user[shops][0][workingTime]'] = this.form['user[shops][0][workingTime]'] || {};
                this.form['user[shops][0][workingTime]'][index] = defaultTime || '';
                if(!value) {
                    delete this.form['user[shops][0][workingTime]'][index]
                }
            }
        }
    });
