/**
 * Created by kozhuhds on 9/11/14.
 */



angular.module('brands.settings')
    .controller('SettingsCtrl', function ($scope, $rootScope, settings, subscriptionTypeShop) {
        settings.initSettingsForm(function (resp) {
            $rootScope.loading = false;
            $scope.isShop = resp.user.shops ? resp.user.shops[0].subscriptionType.internalId == subscriptionTypeShop : false;
        });

        $scope.st = settings;
        $scope.week = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'];
        $scope.time = {};
    })
    .controller('SettingsPassCtrl', function ($scope, $rootScope, settings, $location) {
        $scope.st = settings;
        $rootScope.loading = false;
        $scope.back = function () {
            $location.url('settings');
        }
    });