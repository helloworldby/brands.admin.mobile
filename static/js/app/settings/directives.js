/**
 * Created by kozhuhds on 9/15/14.
 */
angular.module('brands.settings')
    .directive('ngLogoBtn', function (apiUrl, settings, $cookies, $rootScope) {
        return {
            restrict: 'E',
            template: '<button class="add-photo-btn"><i class="fa fa-camera"></i>Загрузить логотип</button>' +
                '<span class="input-file-box"><input type="file" class="imageUpload"></span>',
            link: function (scope, elem, attr) {
                var but = elem.find('button')[0],
                    $input = elem.find('input'),
                    $preview = $('<div class="image-preview_item">' +
                        '<img src=""/>' +
                        '<a href="#" class="remove-preview-btn">' +
                        '<i class="fa fa-times"></i>' +
                        '</a>' +
                        '<div class="shadow-bg">' +
                        '<i class="fa fa-circle-o-notch loading-spinner fa-spin"></i>' +
                        '</div>' +
                        '</div>');
                but.addEventListener('touchstart', function (e) {
                    $input.trigger('click');
                }, false);
                but.addEventListener('click', function (e) {
                    e.preventDefault();
                    $input.trigger('click');
                }, false);


                elem.on('click', '.remove-preview-btn' , function (e) {
                    e.preventDefault();
                    var preview = $(this).parent(),
                        imageName = preview.attr('data-image');

                    delete settings.form['user[shops][0][logo]'];
                    preview.remove();
                });



                $rootScope.$on('shopload', function () {
                    var $img = $preview.find('img');
                    $img.attr('src', settings.logo);
                    $preview.find('.shadow-bg').hide();
                    $preview.find('.remove-preview-btn').css('display','block');
                    settings.form['user[shops][0][logo]'] && $preview.appendTo(elem);
                    if ($img.height() > $img.width()){
                        $img.css('width','100%');
                    }else{
                        $img.css('height','100%');
                    }
                });



                $input.on('change', function (e) {
                    var file = e.target.files.item(0),
                        formData = new FormData(),
                        xhr = new XMLHttpRequest(),
                        reader = new FileReader(),
                        $img = $preview.find('img');


                    formData.append('logo', file);

                    reader.onload = function (e) {
                        $preview.find('img').attr('src', e.target.result);
                        !settings.form['user[shops][0][logo]'] && $preview.appendTo(elem);
                        if ($img.height() > $img.width()){
                            $img.css('width','100%');
                        }else{
                            $img.css('height','100%');
                        }
                    };

                    xhr.onload = function(e) {
                        var resp = JSON.parse(this.responseText);
                        if(this.status == 200 && resp.completed == 1) {
                            $preview.attr('data-image', resp.filename);
                            $preview.find('.shadow-bg').hide();
                            $preview.find('.remove-preview-btn').css('display','block');
                            console.log(resp.filename);
                            settings.form['user[shops][0][logo]'] = resp.filename;
                        }else{
                            console.log('error!!!');
                        }
                    };
                    var token = $cookies.token ? '?access_token=' + $cookies.token.replace(/"/g, "") : '';

                    if(file.type == "image/jpeg" || file.type == "image/png" || file.type == "image/gif"){
                        reader.readAsDataURL(file);
                        xhr.open("POST", apiUrl +  "uploads/shop-logo" + token, true);
                        $preview.find('.shadow-bg').show();
                        xhr.send(formData);
                    }
                });
            }
        }
    });