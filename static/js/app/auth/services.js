/**
 * Created by kozhuhds on 9/8/14.
 */

angular.module('brands.auth').

factory('httpInterceptor', function httpInterceptor ($q, $window, $location, $cookieStore, authUrls) {
    return function (promise) {
        var success = function (response) {
            return response;
        };

        var error = function (response) {
            if (response.status === 401) {
                if(response.data.error == "invalid_grant" && $cookieStore.get("refresh_token")){
                    $cookieStore.remove('token');
                    $location.url('/');
                }else{
                    window.location.href = authUrls.url + '?client_id=' + authUrls.clientID + '&redirect_uri='+ authUrls.redirect +'&response_type=code';
                }
            }
            return $q.reject(response);
        };
        return promise.then(success, error);
    };
}).
factory('authInterceptor', function($cookies) {
    return {
        'request': function(config) {
            var token = $cookies.token ? $cookies.token.replace(/"/g, "") : '';
            config.params = config.params || {};
            config.params['access_token'] = token;
            return config;
        }
    };
});