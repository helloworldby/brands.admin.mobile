/**
 * Created by kozhuhds on 9/8/14.
 */



angular.module('brands.auth')
    .controller('AuthCtrl', function ($scope, $rootScope, auth, $cookies, $location, authUrls) {
        if ($cookies.token) {
            $location.url('/categories');
        }else if ($cookies.refresh_token) {
            auth.refreshToken();
        }else if (window.location.href.split('code').length == 1){
            window.location.href = authUrls.url + '?client_id=' + authUrls.clientID + '&redirect_uri='+ authUrls.redirect +'&response_type=code';
        }else {
            auth.getAccessToken(window.location.href.split('code=')[1].replace('#/','').replace('/',''));
        }
    });