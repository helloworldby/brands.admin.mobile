/**
 * Created by kozhuhds on 9/8/14.
 */



angular.module('brands.auth', [])
    .config(function ($routeProvider) {
        $routeProvider.
            when('/', {
                templateUrl: 'static/js/app/views/auth.html',
                controller: 'AuthCtrl'
            });
    });


angular.module('brands.auth')
    .factory('auth', function ($http, $cookieStore, $location, authUrls) {
        return {
            getAccessToken: function (code) {
                $http.post(authUrls.token, {
                    "client_id": authUrls.clientID,
                    "client_secret": authUrls.clientSecret,
                    "grant_type": 'authorization_code',
                    "redirect_uri": authUrls.redirect,
                    "code": code
                }).success(function (data){
                    //$cookieStore.put('token', data.access_token);
                    //$cookieStore.put('refresh_token', data.refresh_token); 
                    document.cookie = "token=" + data.access_token;
                    window.location.href = authUrls.redirect;
                });
            },
            refreshToken: function () {
                $http.post(authUrls.token, {
                    "client_id": authUrls.clientID,
                    "client_secret": authUrls.clientSecret,
                    "grant_type": 'refresh_token',
                    "refresh_token": $cookieStore.get('refresh_token').replace(/"/g,"")
                }).success(function (data){
                    $cookieStore.put('token', data.access_token);
                    $cookieStore.put('refresh_token', data.refresh_token);
                    window.location.hash = '/#';
                }).error(function () {
                    $cookieStore.remove('token');
                    $cookieStore.remove('refresh_token');
                    window.location.hash = '/#';
                });
            },
            logout: function () {
                $cookieStore.remove('token');
                $cookieStore.remove('refresh_token');
                window.location.href = authUrls.logoutUrl;
            }
        }
    });
