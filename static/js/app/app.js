var brands = angular.module('brands', [
    'mobile-angular-ui',
    'ngRoute',
    'ngResource',
    'ngCookies',

    'brandsDirectives',
    'brandsFilters',
    'brandsServices',
    'brandsConstants',

    'brands.categories',
    'brands.products',
    'brands.auth',
    'brands.settings'
]);


var brandsServices = angular.module('brandsServices', []),
    brandsFilters =  angular.module('brandsFilters', []),
    brandsDirectives =  angular.module('brandsDirectives', []);
    brandsContants =  angular.module('brandsConstants', []);
